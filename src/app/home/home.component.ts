import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { UserService } from '../_services/user.service';
import { NotationArrondissementsService } from "../_services/notation-arrondissements.service";
import { NotationArrondissements } from "../models/notation.arrondissements.model";
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DetailsArrondissementComponent } from '../details-arrondissement/details-arrondissement.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  content: string;
  arrondissements: NotationArrondissements[]=[];
  arrondissement: NotationArrondissements;
  selectedArrondissement: NotationArrondissements;
  msg: string = "";
  //idSend: any = event.target;
  arrByID;
  id;

  constructor(private notationArrondissementsSrv: NotationArrondissementsService, public dialog: MatDialog) { }

  getArrondissementsByNoteGeneraleDecroissante(){
     this.notationArrondissementsSrv.getArrondissementsByNoteGeneraleDecroissante()
     .subscribe(data => {
       this.arrondissements = data;
       console.log(data);
     },
     error => {
      this.msg="Echec de l'affichage des arrondissements";
      console.log(error);
    });
   }

   getArrondissements(){
    this.notationArrondissementsSrv.getAllArrondissements()
    .subscribe(data => {
      this.arrondissements = data;
      console.log(data);
    },
    error => {
     this.msg="Echec de l'affichage des arrondissements";
     console.log(error);
   });
  }

   getArrondissementById(){
     //console.log($event.target.value);
     
    this.notationArrondissementsSrv.getArrondissementByID(this.selectedArrondissement._id)
    .subscribe(data=>{
      this.arrondissement= data;
      console.log(data)
    },
    error => {
      this.msg="Echec de l'affichage de l'arrondissement";
      console.log(error);
    })
    this.openDialog();
  }

  getArrondissementByIdBis(form: NgForm){
    console.log(form.value);
    this.arrByID = form.value['arrByID'];
    console.log(this.arrByID)
    
   this.notationArrondissementsSrv.getArrondissementByID(this.arrByID)
   .subscribe(data=>{
     this.arrondissement= data;
     console.log(data)
     this.onSelect(data)
   },
   error => {
     this.msg="Echec de l'affichage de l'arrondissement";
     console.log(error);
   })
 }

 searchID(){
   console.log(this.id)
  this.notationArrondissementsSrv.getArrondissementByID(this.id)
  .subscribe(data=>{
    this.arrondissement= data;
    console.log(data)
 },
 error => {
   this.msg="Echec de l'affichage de l'arrondissement";
   console.log(error);
 })
}

  openDialog() {
    var index;
    let commentaire
    let arr = this.selectedArrondissement;
    // for (let index = 0; index < this.selectedArrondissement.liste_commentaires.length; index++) {
    //   commentaire = this.selectedArrondissement.liste_commentaires[index];
    // }
      this.dialog.open(DetailsArrondissementComponent, {
        data: {
          _id: arr._id,
          arrondissement: arr.arrondissement,
          photo_arrondissement: arr.photo_arrondissement,
          liste_commentaires: arr.liste_commentaires
          //commentaires: commentaire.negative_comments,
          // lastComment: [
          //   arr.liste_commentaires[0].commentsAt,
          //   arr.liste_commentaires[0].commentsBy,
          //   arr.liste_commentaires[0].negative_comments,
          //   arr.liste_commentaires[0].positive_comments
          // ]
            
        }
      });
   
  }

  onSelect(arr: NotationArrondissements){
    this.selectedArrondissement=arr;
    console.log(JSON.stringify(this.selectedArrondissement));
  }

   ngOnInit():void {
    this.getArrondissementsByNoteGeneraleDecroissante();

  //   this.userService.getPublicContent().subscribe(
  //     data => {
  //       this.content = data;
  //     },
  //     err => {
  //       this.content = JSON.parse(err.error).message;
  //     }
  //   );
   }
}
