import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotationArrondissements } from '../models/notation.arrondissements.model';

const API_URL = 'https://vivre-paris-node-staging.herokuapp.com/vivre-paris/arrondissements';
//const API_URL = 'http://localhost:8080/vivre-paris/arrondissements';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class NotationArrondissementsService {

  constructor(private http: HttpClient) { }

  getAllArrondissements(): Observable<any> {
    return this.http.get(API_URL + '/');
  }

  getArrondissementByID(id): Observable<any> {
    return this.http.get(`${API_URL}/${id}`);
  }

  getArrondissementsByNoteGeneraleDecroissante(): Observable<NotationArrondissements[]> {
    return this.http.get<NotationArrondissements[]>(API_URL+ '/decroissants', httpOptions);
  }

  getArrondissementsWithoutComments(): Observable<NotationArrondissements[]> {
    return this.http.get<NotationArrondissements[]>(API_URL+ '/sans-comments', httpOptions);
  }

  getCommentsOfOneArrondissementByID(id): Observable<any> {
    return this.http.get(`${API_URL}/${id}/liste_commentaires`);
  }
  
}
