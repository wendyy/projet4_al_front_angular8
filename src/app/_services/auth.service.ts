import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from "../models/user.model";

const AUTH_API = 'https://vivre-paris-staging.herokuapp.com/api/auth/';
//const AUTH_API = 'http://localhost:9999/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    }, httpOptions);
  }

  // getAllUsers(): Observable<User[]> {
  //   return this.http.get<User[]>(AUTH_API + 'users', 
  //    httpOptions);
  // }

  // deleteUserById(userId): Observable<any>{
  //   return this.http.delete(AUTH_API + 'user/id/' + userId, 
  //    httpOptions);
  // }

  // deleteUserByUsername(username): Observable<any>{
  //   return this.http.delete(AUTH_API + 'user/id/' + username, 
  //    httpOptions);
  // }

  // updateUser(user: User): Observable<User>{
  //   return this.http.put<User>(AUTH_API + 'user/updatePasswordUsernameEmailWithId/' + user.id, user,
  //    httpOptions);
  // }

  // getUser(username): Observable<User>{
  //   return this.http.get<User>(AUTH_API + 'user/username' + username,
  //    httpOptions);
  // }
}
