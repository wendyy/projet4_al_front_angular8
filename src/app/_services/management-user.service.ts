import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from "../models/user.model";

const AUTH_API = 'https://vivre-paris-staging.herokuapp.com/user/';
//const AUTH_API = 'http://localhost:9999/user/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ManagementUserService {

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(AUTH_API + 'users', 
     httpOptions);
  }

  getUser(username): Observable<User>{
    return this.http.get<User>(AUTH_API + 'username/' + username,
     httpOptions);
  }

  getUserByID(userId): Observable<User>{
    return this.http.get<User>(AUTH_API + 'id/' + userId,
     httpOptions);
  }

  deleteUserById(userId): Observable<any>{
    return this.http.delete(AUTH_API + 'id/' + userId, 
     httpOptions);
  }

  deleteUserByUsername(username): Observable<any>{
    return this.http.delete(AUTH_API + 'username/' + username, 
     httpOptions);
  }

  updateUser(): Observable<User>{
    return this.http.put<User>(AUTH_API + 'id/',
     httpOptions);
  }

  
}
