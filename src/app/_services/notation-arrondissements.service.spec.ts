import { TestBed } from '@angular/core/testing';

import { NotationArrondissementsService } from './notation-arrondissements.service';

describe('NotationArrondissementsService', () => {
  let service: NotationArrondissementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotationArrondissementsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
