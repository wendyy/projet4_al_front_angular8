import { Component, OnInit, ViewChild } from '@angular/core';

import { TokenStorageService } from '../_services/token-storage.service';
import { ManagementUserService } from '../_services/management-user.service';
import { User } from '../models/user.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  currentUser: any;
  //profilUser: User;
  msg: String = "";

  @ViewChild('formUser', { static : false}) 
  form : NgForm ;

  public onFormInit(){

  }

  constructor(private token: TokenStorageService, private managementUserService: ManagementUserService) { }

  ngOnInit() {
    this.currentUser = this.token.getUser();
  }

  updateUser(){
    this.managementUserService.updateUser().subscribe(
      (updatedUser)=>{this.msg="Votre profil a été mis à jour"; },
      (err)=>{console.log(err); this.msg="echec modification/sauvegarde"}
    );
  }


}
