import { Component, OnInit, Inject } from '@angular/core';
import { NotationArrondissementsService } from "../_services/notation-arrondissements.service";
import { NotationArrondissements } from "../models/notation.arrondissements.model";
import { HomeComponent } from "../home/home.component";
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-details-arrondissement',
  templateUrl: './details-arrondissement.component.html',
  styleUrls: ['./details-arrondissement.component.scss']
})
export class DetailsArrondissementComponent implements OnInit {

  selectedArrondissement: NotationArrondissements;
  msg: string = "";
 // idSend = this.arrondissement._id;
 id: String;
 homeComponent: HomeComponent;

  constructor(private notationArrondissementsSrv: NotationArrondissementsService, @Inject(MAT_DIALOG_DATA) public data: NotationArrondissements) { }

  ngOnInit(): void {
    //this.getArrondissementById("01")
  }

  onSelect(arr: NotationArrondissements){
    this.selectedArrondissement=arr
    console.log(JSON.stringify(arr));
  }

  //  getArrondissementById(id: String){
  //   this.notationArrondissementsSrv.getArrondissementByID(id)
  //   .subscribe(data=>{
  //     this.arrondissement= data;
  //     console.log(data)
  //   },
  //   error => {
  //     this.msg="Echec de l'affichage de l'arrondissement";
  //     console.log(error);
  //   })
  // }

}
