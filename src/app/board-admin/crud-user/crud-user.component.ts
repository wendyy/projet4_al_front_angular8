import { Component, OnInit } from '@angular/core';
import { ManagementUserService } from 'src/app/_services/management-user.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-crud-user',
  templateUrl: './crud-user.component.html',
  styleUrls: ['./crud-user.component.scss']
})
export class CrudUserComponent implements OnInit {

  usersList: User[]= [];
  selectedUser: User;
  //idSelected: number;
  confirmDelete :boolean = false;
  mode : string = "newOne" ; //ou "existing"
  msgSaveOrUpdate : string ="";
  msg : string =""; 

  constructor(private managementUserService: ManagementUserService) { }

  getUsers(){
    this.managementUserService.getAllUsers().subscribe(data=>{
      this.usersList=data;
    });
  }

  endOfDelete(){
    this.getUsers();
    //this.onNouvelleDevise();
    this.msg="La suppression a été effectué !"; 
  }

  deleteUser(){
    this.managementUserService.deleteUserById(this.selectedUser.id).subscribe(
      ()=>{this.endOfDelete() },
              (err)=>{this.msg="Echec de la suppression";}
    );
  }

  infoUser(){
    
  }

  onSelect(user: User){
    this.selectedUser = user;
    console.log(JSON.stringify(user));
  }

  ngOnInit(): void {
    this.getUsers();
  }

}
