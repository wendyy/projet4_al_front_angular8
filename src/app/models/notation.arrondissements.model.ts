import { Commentaire } from "../models/commentaires.model";

export class NotationArrondissements {
    public _id :String;
    public arrondissement: String;
    public date_scrappe: Date;
    public liste_commentaires : Array<Commentaire>;
    public note_commerces :Number;
    public note_culture :Number;
    public note_enseignement: Number;
    public note_environnement :Number;
    public note_globale: Number;
    public note_qualite_vie: Number;
    public note_sante: Number;
    public note_securite: Number;
    public note_sports_loisirs: Number;
    public note_transports: Number;
    public photo_arrondissement: String;
    public lien_logement: String;
    public prix_m2: Number;


}