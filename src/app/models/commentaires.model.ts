export class Commentaire {
    public id: Number;
    public commentsBy: String;
    public commentsAt: String;
    public positive_comments: String;
    public negative_comments: String;
    public rating_given_comments: String;
}