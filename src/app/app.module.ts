import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardUserComponent } from './board-user/board-user.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { CrudUserComponent } from './board-admin/crud-user/crud-user.component';
import { DetailsArrondissementComponent } from './details-arrondissement/details-arrondissement.component';

import {MatInputModule  } from "@angular/material/input";
import {MatFormFieldModule } from "@angular/material/form-field";
import { MatAutocompleteModule} from "@angular/material/autocomplete";
import { MatIconModule } from "@angular/material/icon";
import { MatDialogModule} from "@angular/material/dialog";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { FavoriComponent } from './favori/favori.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardUserComponent,
    CrudUserComponent,
    DetailsArrondissementComponent,
    FavoriComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatIconModule,
    MatDialogModule,
    MatProgressBarModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
