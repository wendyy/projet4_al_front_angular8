#FROM node:alpine AS my-app-build
#WORKDIR /app
#COPY . .
#RUN npm ci && npm run build

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
COPY ./dist/ng-front-projet4 /usr/share/nginx/html

