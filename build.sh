#!/bin/sh
if [ $1 = "production" ]
then
    echo "production" 
    sed -i -r "s/.*url.*/        url: $VIVRE_PARIS_API,/g" 
else
    echo "staging"
    sed -i -r "s/.*url.*/        url: $VIVRE_PARIS_STAGING_API,/g" dist/
fi